variable "salt_master_log_level" {
  type = string
  default = "error"
}

variable "salt_minion_log_level" {
  type = string
  default = "info"
}

variable "salt_master_minions" {
  type = list(string)
  default = ["node1.local", "node2.local", "node3.local"]
}
