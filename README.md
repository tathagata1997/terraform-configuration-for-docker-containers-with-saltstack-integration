# Terraform Configuration for Docker Containers with SaltStack Integration



## Introduction

This repository contains Terraform configuration files to set up Docker containers using SaltStack for configuration management. The Terraform configuration consists of the following files:

'main.tf': Main Terraform configuration file
'variables.tf': Variables file for Terraform

## Usage

To apply the Terraform configuration and set up the Docker containers, follow these steps:

### Initialize Terraform:

```
terraform init
```

### Plan the changes:

```
terraform plan
```

### Apply the changes (auto-approve for non-interactive execution):

```
terraform apply -auto-approve
```
The Docker containers 'salt', 'node1.local', 'node2.local', and 'node3.local' will be provisioned according to the specified configuration.

## Docker Container Details

Each container uses a minimal Ubuntu 22.04 base image.
Runs an SSH server allowing password-based login for the 'root' user with the password 'ubuntu'.
Pre-installed packages: openssh-server, curl, sudo.
Locale set to 'en-US', and timezone set to 'Europe/Berlin'.
'salt' container exposes ports 4505 and 4506.

## Test 

The goal of this configuration is to set up 'salt' as a Salt master and 'node1.local', 'node2.local', and 'node3.local' as Salt minions connected to 'salt'. Additionally, set the log file log level of the Salt master to 'error'.

Setup 'jcutnr' as the Salt ID for 'node1.local' and set the log file log level of the corresponding log file to 'debug'.

Setup 'zwsgat' as the Salt ID for 'node2.local' and set the log file log level of the corresponding log file to 'info'.

Setup 'beiqqb' as the Salt ID for 'node3.local' and set the log file log level of the corresponding log file to 'info'.

For any additional details or questions, refer to the 'exercise.md' .
***

