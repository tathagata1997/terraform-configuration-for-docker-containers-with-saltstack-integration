terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.9.0"
    }
  }
}

# Providers
provider "null" {

}

# Set up Salt Master
resource "null_resource" "salt" {
   provisioner "remote-exec" {
      inline = [
         "curl -o bootstrap-salt.sh -L https://bootstrap.saltproject.io",
         "chmod +x bootstrap-salt.sh",
         "./bootstrap-salt.sh -P -M -N stable 3005",
         "sudo service salt-master start",
         "echo 'auto_accept: True' >> /etc/salt/master",
         "echo 'log_level_logfile: error' >> /etc/salt/master",
         "sudo pkill salt-master",
         "sudo service salt-master restart",
         "sudo salt-key -a jcutnr -y",
         "sudo salt-key -a zwsgat -y",
         "sudo salt-key -a beiqqb -y"
      ]
 connection {
    type     = "ssh"
    user     = "root"
    password = "ubuntu"
    host     = "salt"
   }
 }
}

# Set up Salt Minion for Node 1
resource "null_resource" "node1" {
 provisioner "remote-exec" {
    inline = [
      "sudo apt-get install -y salt-minion",
      "service salt-minion start",
      "echo 'master: salt' > /etc/salt/minion.d/master.conf",
      "echo 'log_level_logfile: debug' > /etc/salt/minion.d/log_level.conf",
      "echo 'id: jcutnr' > /etc/salt/minion.d/id.conf",
      "service salt-minion restart"
    ]

 connection {
    type     = "ssh"
    user     = "root"
    password = "ubuntu"
    host     = "node1.local"
    }
 }
}

# Set up Salt Minion for Node 2
resource "null_resource" "node2" {
 provisioner "remote-exec" {
    inline = [
      "sudo apt-get install -y salt-minion",
      "service salt-minion start",
      "echo 'master: salt' > /etc/salt/minion.d/master.conf",
      "echo 'log_level_logfile: info' > /etc/salt/minion.d/log_level.conf",
      "echo 'id: zwsgat' > /etc/salt/minion.d/id.conf",
      "service salt-minion restart"
    ]
 connection {
    host     = "node2.local"
    type     = "ssh"
    user     = "root"
    password = "ubuntu"
    }
 }
}

# Set up Salt Minion for Node 3
resource "null_resource" "node3" {
 provisioner "remote-exec" {
    inline = [
      "sudo apt-get install -y salt-minion",
      "service salt-minion start",
      "echo 'master: salt' > /etc/salt/minion.d/master.conf",
      "echo 'log_level_logfile: info' > /etc/salt/minion.d/log_level.conf",
      "echo 'id: beiqqb' > /etc/salt/minion.d/id.conf",
      "service salt-minion restart"
    ]
 connection {
    host     = "node3.local"
    type     = "ssh"
    user     = "root"
    password = "ubuntu"
    }
 }
}